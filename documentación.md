**Show User**
----
  Returns json data about a single user.

* **URL**

    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/','TestController@welcome');
    Route::post('/store','TestController@store');//lista
    Route::post('/destroy/{id}','TestController@destroy');//eliminar
    Route::post('/update/{id}','TestController@update');//actualizar
    Route::get('/edit/{id}','TestController@edit');//editar
    Route::get('/info/{id}','TestController@inf');//ver información

* **Method:**

  `GET` | `POST`
  
*  **URL Params**

   **Required:**
 
   `id=[integer]`

* **Data Params**

  {
    {
    
    nombre: [cadena],
    email: [cadena],
    password: [alfanumérico],
    password_confirmación: [alfanumérica],
    admin: [boolean],
  }
}

* **Success Response:**

  * **Code:** 200 <br />
    **Content:** `{ id : 12, name : "Michael Bloom"  : "Michel@gmail.com" : "12345" : "0"}`
 
* **Error Response:**

 Se tiene control de errores

* **Sample Call:**

 None