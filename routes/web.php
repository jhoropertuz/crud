<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/home', 'HomeController@index')->name('home');
Route::get('/','TestController@welcome');
/*Route::get('/', function(){

	$student=App\Student::find(1);

	return  $student->courses()->attach(1);;
});*/
//estudiante
Route::get('/student/create','StudentController@create');//crear
Route::post('/student/store','StudentController@store');//lista
Route::post('/student/destroy/{id}','StudentController@destroy');//eliminar
Route::post('/student/update/{id}','StudentController@update');//actualizar
Route::get('/student/edit/{id}','StudentController@edit');//editar
Route::get('/student/info/{id}','StudentController@inf');//ver información

//docente
Route::get('/teacher/create','TeacherController@create');//crear
Route::post('/teacher/store','TeacherController@store');//lista
Route::post('/teacher/destroy/{id}','TeacherController@destroy');//eliminar
Route::post('/teacher/update/{id}','TeacherController@update');//actualizar
Route::get('/teacher/edit/{id}','TeacherController@edit');//editar
Route::get('/teacher/info/{id}','TeacherController@inf');//ver información

//curso
Route::get('/course/create','CourseController@create');//crear
Route::post('/course/store','CourseController@store');//lista
Route::post('/course/destroy/{id}','CourseController@destroy');//eliminar
Route::post('/course/update/{id}','CourseController@update');//actualizar
Route::get('/course/edit/{id}','CourseController@edit');//editar
Route::get('/course/info/{id}','CourseController@inf');//ver información
Auth::routes();


/*
  <form method="post"  class="row"action="{{url('/store')}}">
                                {{csrf_field()}}
                                  <div class="col-12 mt-2  text-center">
                                      Crear Usuario
                                  </div>
                                  <div class="form-group col-6  mt-2">
                                    <input type="text" class="form-control" placeholder="Nombre" name="name" required>
                                  </div>
                                  <div class="form-group col-6 mt-2">
                                    <input type="email" class="form-control " id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Email" name="email" required>
                                    
                                  </div>
                                  <div class="form-group  col-12">
                                    <input type="password" class="form-control" id="exampleInputPassword1" placeholder="Contraseña" name="password" required>
                                  </div>
                                <div class="text-center col-12">
                                     <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                                 
                                </form>*/