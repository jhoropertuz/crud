@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">CRUD</div>

                <div class="card-body">
                    <div class="content">
                          <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Editar</a>
                              </li>
                             
                          </ul>
                          <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    
                                  <form method="post"  class="row"action="{{url('/update/'.$user->id.'')}}">
                                    {{csrf_field()}}
                                      <div class="col-12 mt-2  text-center">
                                          Editar Usuario {{$user->name}}
                                      </div>
                                      <div class="form-group col-12 mt-2">
                                        <input type="text" class="form-control" value="{{$user->name}}" name="name" required>
                                      </div>
                                      <div class="form-group col-12 mt-2">
                                        <input type="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" value="{{$user->email}}" name="email"required>
                                      </div>
                                      <div class="form-group  col-12">
                                        <input type="password" class="form-control" id="exampleInputPassword1" value="" placeholder="Contraseña" name="password" required>
                                      </div>
                                    <div class="text-center col-12">
                                         <a class="btn btn-danger btn "  title = "Atras" href="{{url('/home')}}">Atras</a>
                                          <button type="submit" class="btn btn-primary">actualizar</button>
                                    </div>
                                     
                                    </form>
                                </div>
                          
                             
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
