              
@extends('layouts.app')

@section('content')
                @if (session('mensaje'))
                        <div class="alert alert-danger text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif

           <div class="row justify-content-center">
            
                    <div class="card text-center ">
  <div class="card-header">
    Crear Curso
  </div>
  <div class="card-body">
    
     <form method="post"  class="row"action="{{url('/course/store')}}">
                                {{csrf_field()}}

                                  <div class="form-group col-6 mt-2">
                                    <input type="number" class="form-control "  placeholder="Codigo" name="code" required>
                                    
                                  </div>
                                  <div class="form-group col-6  mt-2">
                                    <input type="text" class="form-control" placeholder="Nombre" name="name" required>
                                  </div>
                                  <div class="form-group col-12 mt-2">
                                    <input type="text" class="form-control "  placeholder="Observación" name="observation" required>
                                    
                                  </div>
                                  
                                  
                                  <div class="col-6 mb-5">
                                    <label for="exampleFormControlSelect1">Asignar Docente</label>
                                    <select class="browser-default custom-select mt-2" name="teacher_id" id="exampleFormControlSelect1">
                                         @foreach ($teachers as $key=>$teacher)
                                        <option value="{{$teacher->id}}" selected>{{$teacher->name}}</option>
                                        @endforeach
                                      </select>
                                  </div> 
                                               


                                  <div class="text-center col-12">
                                     
                                     <a href="{{url('/home')}}" class="btn btn-danger">Salir</a>
                                      <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                                
                                 
              </form>
  </div>
  
</div>
 </div>             

@endsection