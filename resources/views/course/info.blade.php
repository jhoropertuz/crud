@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <img id="profile-img" class="profile-img-card mt-2" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <div class="card-header text-center"><strong>{{$course->name}} </strong></div>

                <div class="card-body">
                    <div class="content text-center">
                      <h5><strong>Observación:</strong> {{$course->observation}}</h5><br>
                      <h5><strong>Docente</strong> 
                        
                                        @foreach ($teachers as $key=>$teacher)
                                        @if($teacher->id==$course->teacher_id)
                                        {{$teacher->name}}
                                        
                                        @endif
                                        @endforeach

                                    </h5><br>
                        <a class="btn btn-danger  mt-5"  title = "Atras" href="{{url('/home')}}">Atras
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
  
  .profile-img-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
</style>