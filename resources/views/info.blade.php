@extends('layouts.app')

@section('content')

<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <img id="profile-img" class="profile-img-card mt-2" src="//ssl.gstatic.com/accounts/ui/avatar_2x.png" />
                <div class="card-header text-center"><strong>{{$user->name}}</strong></div>

                <div class="card-body">
                    <div class="content text-center">
                          
                      <h5>{{$user->email}}</h5>
                        <a class="btn btn-danger btn-sm mt-5"  title = "Atras" href="{{url('/home')}}">Atras
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
<style type="text/css">
  
  .profile-img-card {
    width: 96px;
    height: 96px;
    margin: 0 auto 10px;
    display: block;
    -moz-border-radius: 50%;
    -webkit-border-radius: 50%;
    border-radius: 50%;
}
</style>