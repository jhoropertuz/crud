              
@extends('layouts.app')

@section('content')
                @if (session('mensaje'))
                        <div class="alert alert-danger text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif

           <div class="row justify-content-center">
            
                    <div class="card text-center ">
  <div class="card-header">
    Crear Docente
  </div>
  <div class="card-body">
    
     <form method="post"  class="row"action="{{url('teacher/store')}}">
                                {{csrf_field()}}
                                  
                                  <div class="form-group col-6  mt-2">
                                    <input type="text" class="form-control" placeholder="Nombre" name="name" required>
                                  </div>
                                  <div class="form-group col-6 mt-2">
                                    <input type="text" class="form-control "  placeholder="Apellido" name="surname" required>
                                    
                                  </div>
                                  <div class="form-group col-6 mt-2">
                                    <input type="number" class="form-control "  placeholder="Identificación" name="cc" required>
                                    
                                  </div>

                                  <div class="col-6">
                                    <select class="browser-default custom-select mt-2" name="gender">
                                        
                                        <option value="m" selected>Masculino</option>
                                        <option value="f">Femenino</option>
                                        <option value="o">Otros</option>
                                      </select>
                                  </div> 
                                  


                                  <div class="text-center col-12">
                                     
                                     <a href="{{url('/home')}}" class="btn btn-danger">Salir</a>
                                      <button type="submit" class="btn btn-primary">Crear</button>
                                </div>
                                
                                 
              </form>
  </div>
  
</div>
 </div>             

@endsection