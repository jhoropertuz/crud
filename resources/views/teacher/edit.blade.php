@extends('layouts.app')

@section('content')
@if (session('mensaje'))
                        <div class="alert alert-danger text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif
   @if (session('mensaje2'))
                        <div class="alert alert-success text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje2') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">Actualizar Datos</div>

                <div class="card-body">
                    <div class="content">
                          <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Editar Docente {{$teacher->name}}</a>
                              </li>
                             
                          </ul>
                          <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                    
                                  <form method="post"  class="row"action="{{url('teacher/update/'.$teacher->id.'')}}">
                                    {{csrf_field()}}
                                      
                                      <div class="form-group col-6 mt-2">
                                        <input type="text" class="form-control" value="{{$teacher->name}}" name="name" required>
                                      </div>
                                      <div class="form-group col-6 mt-2">
                                        <input type="text" class="form-control"  value="{{$teacher->surname}}" name="surname"required>
                                      </div>
                                      <div class="form-group col-6 mt-2">
                                    <input type="number" class="form-control "  value="{{$teacher->cc}}"placeholder="Identificación" name="cc" required>
                                    
                                  </div>
                                      <div class="col-6">
                                    <select class="browser-default custom-select mt-2" name="gender">
                                    
                                        <option value="Maculino" selected>Masculino</option>
                                        <option value="Femenino">Femenino</option>
                                        <option value="Otros">Otros</option>
                                      </select>
                                      
                                  </div> 
                                 

                                  <div class="text-center col-12">
                                     
                                     <a href="{{url('/home')}}" class="btn btn-danger">Salir</a>
                                      <button type="submit" class="btn btn-warning">Actualizar</button>
                                </div>
                                
                                    </form>
                                </div>
                          
                             
                            </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
