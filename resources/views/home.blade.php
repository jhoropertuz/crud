@extends('layouts.app')

@section('content')
 @if (session('mensaje'))
                        <div class="alert alert-danger text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif
   @if (session('mensaje2'))
                        <div class="alert alert-success text-center  my-0 pb-1 pt-1" style="position:absolute; z-index: 1; top: 9%; width: 100%; left: 0%;  ">
                            <a class="py-0 my-0 ">{{ session('mensaje2') }}</a>
                             <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                              </button>
                        </div>
                    @endif
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header text-center"><h4>Escritorio</h4></div>

                <div class="card-body">
                    <div class="content">
                        
                          <ul class="nav nav-tabs" id="myTab" role="tablist">
                              <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Estudiantes</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Docentes</a>
                              </li>
                              <li class="nav-item">
                                <a class="nav-link" id="profile2-tab" data-toggle="tab" href="#profile2" role="tab" aria-controls="profile2" aria-selected="false">Cursos</a>
                              </li>
                             
                            </ul>
                            <div class="tab-content" id="myTabContent">
                              <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                                  <a   title = "Crear" class = "btn btn-dark  btn-sm mt-5 mb-3" href="{{url('/student/create')}}" role="button">Crear Estudiante</a>
                                  <div class="table-responsive ">
                            <table  class="table table-hover" id="">
                                <thead>
                                    <tr class=" shadow-md p-3 mb-5 bg-warning  rounded text-center">
                                        <th class="px-0 ">No.</th>
                                        <th>Nombre</th>
                                        <th class="px-4" style="padding-left: 50px; padding-right: 50px;">Apellido</th>
                                        <th class="px-4" style="padding-left: 50px; padding-right: 50px;">Genero</th>
                                        <th class="" style="padding-left: 50px; padding-right: 50px;">Ajustes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @foreach ($students as $key=>$student)
                                    <tr class="odd gradeX text-center">
                                        
                                        <td>{{$key+1}}</td>
                                        <td>{{$student->name}}</td>
                                         <td>{{$student->surname}}</td>
                                         <td>{{$student->gender}}</td>
                                        <td class="text-center">
                                             <form method="post" action="{{url('/student/destroy/'.$student->id.'')}}" >
                                                 {{csrf_field()}}
                                                  
                                                  <a   title = "Info" class = "btn btn-dark  btn-sm " href="{{url('/student/info/'.$student->id.'')}}" role="button"><i class = "fa fa-info" ></i></a>
                                                    <a   title = "Editar"  class = "btn btn-success  btn-sm " href="{{url('/student/edit/'.$student->id.'')}}" role="button"><i class = "fa fa-edit" ></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"  title = "Eliminar" >

                                                <i class = "fa fa-times" ></i>
                                             </button>
                                             </form>
                                          
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                           

                            </div>
                           
                              </div>
                              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">

                                  <a   title = "Crear" class = "btn btn-dark  btn-sm mt-5 mb-3" href="{{url('/teacher/create')}}" role="button">Crear Docente</a>

                                  <div class="table-responsive ">
                            <table  class="table table-hover" id="">
                                <thead>
                                    <tr class=" shadow-md p-3 mb-5 bg-warning  rounded text-center">
                                        <th class="px-0 ">No.</th>
                                        <th>Cursos</th>
                                        <th>Nombre</th>
                                        <th class="px-4" style="padding-left: 50px; padding-right: 50px;">Apellido</th>
                                        <th class="px-4" style="padding-left: 50px; padding-right: 50px;">Genero</th>
                                        <th class="" style="padding-left: 50px; padding-right: 50px;">Ajustes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @foreach ($teachers as $key=>$teacher)
                                    <tr class="odd gradeX text-center">
                                        
                                        <td>{{$key+1}}</td>
                                        <td>
                                          
                                            @foreach ($teacher->courses()->get() as $key=>$tea)
                                             
                                              {{$tea->name}}
                                    
                                            @endforeach

                                           
                                         </td>
                                        <td>{{$teacher->name}}</td>
                                         <td>{{$teacher->surname}}</td>

                                         <td>{{$teacher->gender}}</td>
                                        <td class="text-center">
                                             <form method="post" action="{{url('/teacher/destroy/'.$teacher->id.'')}}" >
                                                 {{csrf_field()}}
                                                  
                                                  <a   title = "Info" class = "btn btn-dark  btn-sm " href="{{url('/teacher/info/'.$teacher->id.'')}}" role="button"><i class = "fa fa-info" ></i></a>
                                                    <a   title = "Editar"  class = "btn btn-success  btn-sm " href="{{url('/teacher/edit/'.$teacher->id.'')}}" role="button"><i class = "fa fa-edit" ></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"  title = "Eliminar" >

                                                <i class = "fa fa-times" ></i>
                                             </button>
                                             </form>
                                          
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                           

                            </div>

                              </div>

                              <div class="tab-pane fade" id="profile2" role="tabpanel" aria-labelledby="profile2-tab">
                                <a   title = "Crear" class = "btn btn-dark  btn-sm mt-5 mb-3" href="{{url('/course/create')}}" role="button">Crear Curso</a>

                                  <div class="table-responsive ">
                            <table  class="table table-hover" id="">
                                <thead>
                                    <tr class=" shadow-md p-3 mb-5 bg-warning  rounded text-center">
                                        <th class="px-0 ">No.</th>
                                        <th>Nombre</th>
                                        <th class="px-4" style="padding-left: 50px; padding-right: 50px;">Observacion</th>
                                        
                                        <th class="" style="padding-left: 50px; padding-right: 50px;">Ajustes</th>
                                    </tr>
                                </thead>
                                <tbody>
                                     @foreach ($courses as $key=>$course)
                                    <tr class="odd gradeX text-center">
                                        
                                        <td>{{$key+1}}</td>
                                        <td>{{$course->name}}</td>
                                         <td>{{$course->observation}}</td>
                                        
                                        <td class="text-center">
                                             <form method="post" action="{{url('/course/destroy/'.$course->id.'')}}" >
                                                 {{csrf_field()}}
                                                  
                                                  <a   title = "Info" class = "btn btn-dark  btn-sm " href="{{url('/course/info/'.$course->id.'')}}" role="button"><i class = "fa fa-info" ></i></a>
                                                    <a   title = "Editar"  class = "btn btn-success  btn-sm " href="{{url('/course/edit/'.$course->id.'')}}" role="button"><i class = "fa fa-edit" ></i></a>
                                                    <button type="submit" class="btn btn-danger btn-sm"  title = "Eliminar" >

                                                <i class = "fa fa-times" ></i>
                                             </button>
                                             </form>
                                          
                                        
                                        </td>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                           

                            </div>
                              </div>
                             
                            </div>
                            
                             
                           
                            
                           
                            
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


