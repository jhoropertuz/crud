<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Student;
use App\Teacher;
class CourseController extends Controller
{
  public function create(Request $request)
    {
      $teachers=teacher::get();
      return view('course.create')->with(compact('teachers'));
    }
	public function store(Request $request)
    {
      $course=new Course();
      $course->name=$request->input('name');
      $course->code=$request->input('code');
      $course->observation=$request->input('observation');
      $course->teacher_id=$request->input('teacher_id');
      //validar que el email de usuario no este registrado.
      $contador=Course::where('code',$course->code);
      $mensaje="Codigo ya esta registrado.";
      $mensaje2="Curso creado con exitó";
      if($contador->count()==1)
      {
      	return back()->with(compact('mensaje'));
      }
      else
      {
      	$course->save();

      	return redirect("/home")->with(compact('mensaje2'));
      }
            
    }
    //inyectar datos de usuario
    public function inf($id)
    {
      
      $teachers=teacher::get();
      $course=Course::find($id);
      return view('course.info')->with(compact('teachers','course'));
    }

    //inyectar datos de usuario a editar
    public function edit($id)
    {

      $teachers=teacher::get();
      $course=Course::find($id);
      return view('course.edit')->with(compact('teachers','course'));
    }

    //actualizar datos de usuario a editar
    public function update(Request $request,$id)
    {
      //dd($request->all());
      $course=Course::find($id);
      $course->delete();
      $course=new Course();
      $course->name=$request->input('name');
      $course->code=$request->input('code');
      $course->observation=$request->input('observation');
      $course->teacher_id=$request->input('teacher_id');
      //validar que el email de usuario no este registrado.
      $contador=Course::where('code',$course->code);
      $mensaje="Codigo ya esta registrado.";
      if($contador->count()==1)
      {
      	return back()->with(compact('mensaje'));
      }
      else
      {
      	$course->save();
      	return redirect("/home")->with(compact('mensaje2'));
      }
      
    }

    //eliminar datos de un usuario
    public function destroy($id)
    {
      $course=Course::find($id);
      $course->delete();

      return back();
    }
}
