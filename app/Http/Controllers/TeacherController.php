<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Student;
use App\Teacher;
class TeacherController extends Controller
{
    public function create(Request $request)
    {
      $courses=Course::get();
      return view('teacher.create')->with(compact('courses'));
    }
    //creear usuario
    public function store(Request $request)
    {
      $teacher=new Teacher();
      $teacher->name=$request->input('name');
      $teacher->surname=$request->input('surname');
      $teacher->gender=$request->input('gender');
      $teacher->cc=$request->input('cc');
      //validar que el email de usuario no este registrado.
      $contador=Teacher::where('cc',$teacher->cc);
      $mensaje="Identificacion ya esta registrada.";
      $mensaje2="Docente creado con exitó";
      if($contador->count()==1){
      	return back()->with(compact('mensaje'));
      }
      else
      {
      	$teacher->save();
      	return redirect("/home")->with(compact('mensaje2'));
      }
            
    }
    //inyectar datos de usuario
    public function inf($id)
    {
      $teacher=Teacher::find($id);
      
      return view('teacher.info')->with(compact('teacher'));
    }

    //inyectar datos de usuario a editar
    public function edit($id)
    {
      $teacher=Teacher::find($id);

      return view('teacher.edit')->with(compact('teacher'));
    }

    //actualizar datos de usuario a editar
    public function update(Request $request,$id)
    {
      //dd($request->all());
      $teacher=Teacher::find($id);
      $teacher->delete();
      $teacher=new teacher();
      $teacher->name=$request->input('name');
      $teacher->surname=$request->input('surname');
      $teacher->cc=$request->input('cc');
      $teacher->gender=$request->input('gender');
      //validar que el email de usuario no este registrado.
      $contador=teacher::where('cc',$teacher->cc);
      $mensaje="Identificacion ya esta registrada.";
      $mensaje2="Docente actualizado con exitó";
      if($contador->count()==1)
      {
      	return back()->with(compact('mensaje'));
      }
      else
      {
      	$teacher->save();
      	return redirect("/home")->with(compact('mensaje2'));;
      }
      
    }

    //eliminar datos de un usuario
    public function destroy($id)
    {
      $teacher=Teacher::find($id);
      $teacher->delete();

      return back();
    }
}
