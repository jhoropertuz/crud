<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Course;
use App\Student;
use App\Teacher;
class StudentController extends Controller
{   

    public function create(Request $request)
    {
      $courses=Course::get();
      return view('student.create')->with(compact('courses'));
    }
    //creear usuario
    public function store(Request $request)
    {
      $student=new Student();
      $student->name=$request->input('name');
      $student->surname=$request->input('surname');
      $student->cc=$request->input('cc');
      $student->gender=$request->input('gender');
      //validar que el email de usuario no este registrado.
      $contador=Student::where('cc',$student->cc);
      $mensaje="Identificacion ya esta registrada.";
      $mensaje2="Estudiante creado con exitó";
      $varcourses=$request->input('courses');
      
      
      $teachers = Teacher::orderBy('id','desc')->get();
      $students = Student::orderBy('id','desc')->get();
      $courses = Course::orderBy('id','desc')->get();

      if($contador->count()==1 )
      {
      	return back()->with(compact('mensaje'));
      }
      else
      {
        
        if (is_null($varcourses)) {
            
            $student->save();
            return redirect("/home")->with(compact('teachers','students','courses','mensaje2'));
        }else{
            $student->save();
         foreach ($varcourses as $course) {
                $curso=Course::find($course);
                $student->courses()->attach($curso);
                return redirect("/home")->with(compact('teachers','students','courses','mensaje2'));
            }
        }
      
      	
      }
            
    }
    //inyectar datos de usuario
    public function inf($id)
    {
      $student=Student::find($id);
      
      return view('student.info')->with(compact('student'));
    }

    //inyectar datos de usuario a editar
    public function edit($id)
    {
      $student=Student::find($id);
      $courses = Course::orderBy('id','desc')->get();
      return view('student.edit')->with(compact('student','courses'));
    }

    //actualizar datos de usuario a editar
    public function update(Request $request,$id)
    {
      //dd($request->all());
      $student=Student::find($id);
      $student->delete();
      $student=new Student();
      $student->name=$request->input('name');
      $student->surname=$request->input('surname');
      $student->cc=$request->input('cc');
      $student->gender=$request->input('gender');
      //validar que el email de usuario no este registrado.
      $contador=Student::where('cc',$student->cc);
      $mensaje="Identificacion ya esta registrada.";
      $mensaje2="Estudiante actualizado con exitó";
      $varcourses=$request->input('courses');
      
      
      $teachers = Teacher::orderBy('id','desc')->get();
      $students = Student::orderBy('id','desc')->get();
      $courses = Course::orderBy('id','desc')->get();

      if($contador->count()==1 )
      {
        return back()->with(compact('mensaje'));
      }
      else
      {
        
        if (is_null($varcourses)) {
            
            $student->save();
            return redirect("/home")->with(compact('teachers','students','courses','mensaje2'));
        }else{
            $student->save();
         foreach ($courses as $course) {
                $curso=Course::find($course);
                $student->courses()->attach($curso);
                return redirect("/home")->with(compact('teachers','students','courses','mensaje2'));
            }
        }
      
        
      }}

    //eliminar datos de un usuario
    public function destroy($id)
    {
      $student=Student::find($id);
      $student->delete();

      return back();
    }
}
