<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Teacher;
use App\Student;
use App\Course;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        //inyectar tabla de usuarios a la vista home.
        $teachers = Teacher::orderBy('id','desc')->get();
        $students = Student::orderBy('id','desc')->get();
        $courses = Course::orderBy('id','desc')->get();
        return view('home')->with(compact('teachers','students','courses'));
        //return view('home');
    }
}
