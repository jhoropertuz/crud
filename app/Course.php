<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Student;
use App\Teacher;
class Course extends Model
{

      public function students()
    {
        return $this->belongsToMany(Student::class);
    }
    public function teachers()
    {
        return $this->belongsTo(Teacher::class);
    }

}
