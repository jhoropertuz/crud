<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;
class Student extends Model
{
      public function courses()
    {
        return $this->belongsToMany(Course::class);//muchos a muchos
    }
}
