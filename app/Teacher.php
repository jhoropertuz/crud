<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Course;
class Teacher extends Model
{
     public function courses()
    {
        return $this->hasMany(Course::class);
    }
}
